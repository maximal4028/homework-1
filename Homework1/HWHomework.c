//
//  HWHomework.c
//  Homework1
//
//  Created by Vladislav Grigoriev on 21/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#include "HWHomework.h"
#include "stdlib.h"
#include <math.h>
#include <time.h>
#include <string.h>
/*
 * Задание 1. (1 балл)
 *
 * Необходмо реализовать метод, который создаст матрицу и заполнит её случайными числами от -100 до 100.
 *
 * Параметры:# *      rows - количество строк в матрице;
 *      columns - количество столбцов в матрице;
 *
 * Возвращаемое значение:
 *      Матрица размером [rows, columns];
 */
long** createMatrix(const long rows, const long columns)
{
    srand(time(NULL));
    long **matrix = (long**)malloc(rows*sizeof(long*));
    for (int i = 0; i<rows; i++)
    {
        matrix[i] = (long*)malloc(columns*sizeof(long));
        for (int j = 0; j<columns; j++)
            matrix[i][j] = -100 + rand() % 201;
    }
    return matrix;

}
/*
 * Задание 2.
 *
 * Необходимо реализовать метод, котрый преобразует входную строку, состоящую из чисел, в строку состоящую из числительных.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      numberString - входная строка (например: "1", "123.456");
 *
 * Возвращаемое значение:
 *      Обычный уровень сложности (1 балл):
 *             Строка, содержащая в себе одно числительное или сообщение о невозможности перевода;
 *             Например: "один", "Невозможно преобразовать";
 *
 *      Высокий уровень сложности (2 балла):
 *              Строка, содержащая себе числительные для всех чисел или точку;
 *              Например: "один", "один два три точка четыре пять шесть";
 *
 *
 */
char* stringForNumberString(const char *numberString)
{
    char * NewString = (char*)malloc(2*sizeof(numberString)*sizeof(char));
    char * LastString = (char*)malloc(32*sizeof(numberString)*sizeof(char));
    int i = 0;

    do{
            switch(numberString[i])
            {
                case '1':
                {
                    NewString = "один ";
                    break;
                }
                case '2':
                {
                    NewString = "два ";
                    break;
                }
                case '3':
                {
                    NewString = "три ";
                    break;
                }
                case '4':
                {
                    NewString = "четыре ";
                    break;
                }
                case '5':
                {
                    NewString = "пять ";
                    break;
                }
                case '6':
                {
                    NewString = "шесть ";
                    break;
                }
                case '7':
                {
                    NewString = "семь ";
                    break;
                }
                case '8':
                {
                    NewString = "восемь ";
                    break;
                }
                case '9':
                {
                    NewString = "девять ";
                    break;
                }
                case '0':
                {
                    NewString = "ноль ";
                    break;
                }
                case '.':
                {
                    NewString = "точка ";
                    break;
                }
                default:
                    NewString = "Невозможно преобразовать";
                    break;
            }
    i++;
    strcat(LastString,NewString);
    } while (numberString[i] != '\0');
    return LastString;
}



/*
 * Задание 3.
 *
 * Необходимо реализовать метод, который возвращает координаты точки в зависимости от времени с начала анимации.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      time - время с начала анимации в секундах (например: 0 или 1.234);
 *      canvasSize - длина стороны квадрата, на котором происходит рисование (например: 386);
 *
 * Возвращаемое значение:
 *      Структура HWPoint, содержащая x и y координаты точки;
 *
 * Особенность: 
 *      Начало координат находится в левом верхнем углу.
 *
 * Обычный уровень сложности (2 балла):
 *      Анимация, которая должна получиться представлена в файле Default_mode.mov.
 *
 * Высокий уровень сложности (3 балла):
 *      Анимация, которая должна получиться представлена в файле Hard_mode.mov.
 *
 * PS: Не обязательно, чтобы ваша анимация один в один совпадала с примером. 
 * PPS: Можно реализовать свою анимацию, уровень сложности которой больше или равен анимациям в задании.
 */
HWPoint pointForAnimationTime(const double time, const double canvasSize) {
    return (HWPoint){canvasSize / 2.0, canvasSize / 2.0};
}

